# n2fa [![pipeline status](https://git.metaa.io/npm/n2fa/badges/master/pipeline.svg)](https://git.metaa.io/npm/n2fa/commits/master)

A simple OTP token generator for the commandline.

## Usage
`n2fa ~/.config/2fa.json`

Ideally you would want set this to be an alias command, like for example:  
`alias 2fa='n2fa ~/.config/2fa.json'`

## Configuration
This is an example config file:
```json
[
    {
        "service": "Google",
        "accounts": {
            "Default": "01234567890123456789012345678901",
            "Other": "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345",
        }
    }
]
```
